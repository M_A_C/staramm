//
//  main.m
//  VimgKeditor
//
//  Created by Илья on 20.09.12.
//  Copyright (c) 2012 ilya.sergeev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VIAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([VIAppDelegate class]));
	}
}
