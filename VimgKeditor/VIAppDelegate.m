//
//  VIAppDelegate.m
//  VimgKeditor
//
//  Created by Илья on 20.09.12.
//  Copyright (c) 2012 ilya.sergeev. All rights reserved.
//

#import "VIAppDelegate.h"

#import "VIFirstViewController.h"
#import "VIFiltersFactory.h"

@implementation VIAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	[VIFiltersFactory prepareFiltersPreviewCompleteBlock:nil];
	
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	VIFirstViewController* mainViewController = [[VIFirstViewController alloc] initWithNibName:@"VIFirstViewController" bundle:nil];
	UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:mainViewController];
	navigationController.navigationBarHidden = YES;
	self.viewController = navigationController;
	self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

@end
