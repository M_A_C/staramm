//
//  VIAppDelegate.h
//  VimgKeditor
//
//  Created by Илья on 20.09.12.
//  Copyright (c) 2012 ilya.sergeev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong) UIViewController* viewController;

@end
