//
//  UIImage_resizable.m
//  VimgKeditor
//
//  Created by Илья on 21.09.12.
//  Copyright (c) 2012 ilya.sergeev. All rights reserved.
//

#import "UIImage_resizable.h"

@implementation UIImage(resizable)

+ (UIImage*)resizableImageWithName:(NSString*)name
{
	UIImage* image = [UIImage imageNamed:name];
	CGSize size = image.size;
	UIEdgeInsets insets = UIEdgeInsetsMake(size.height/2, size.width/2, size.height/2, size.width/2);
	float version = [[[UIDevice currentDevice] systemVersion] floatValue];
	if (version >= 5)
	{
		return [image resizableImageWithCapInsets:insets];
	}
	return [image stretchableImageWithLeftCapWidth:insets.left topCapHeight:insets.top];
}

- (UIImage*)imageScaledToSize:(CGSize)preferSize
{
	float xScaleFactor = self.size.width/preferSize.width;
	float yScaleFactor = self.size.height/preferSize.height;
	float previewScale = floorf(MIN(xScaleFactor, yScaleFactor));
	
	return [UIImage imageWithCGImage:self.CGImage
							   scale:previewScale
						 orientation:self.imageOrientation];
}

@end
