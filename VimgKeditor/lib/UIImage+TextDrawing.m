//
//  UIImage+TextDrawing.m
//  VimgKeditor
//
//  Created by Илья on 30.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "UIImage+TextDrawing.h"

@implementation UIImage(TextDrawing)

-(UIImage *)imageWithText:(NSString *)text
				 withFont:(UIFont*)font
					color:(UIColor*)color
				   inRect:(CGRect)rect
				alignment:(NSTextAlignment)alignment
{
    UIGraphicsBeginImageContext(self.size);
    [self drawInRect:CGRectMake(0, 0, self.size.width, self.size.height)];
    [color set];
    [text drawInRect:CGRectIntegral(rect)
			withFont:font
	   lineBreakMode:NSLineBreakByTruncatingTail
		   alignment:alignment];
	
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return newImage;
}

-(UIImage *)imageWithMaskImage:(UIImage*)maskImage
{
	UIGraphicsBeginImageContext(self.size);
	CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    [self drawInRect:rect];
	
	float scale = MIN(maskImage.size.width/self.size.width, maskImage.size.height/self.size.height);
	UIImage* newMaskImage = [UIImage imageWithCGImage:maskImage.CGImage
												scale:scale
										  orientation:maskImage.imageOrientation];
	[newMaskImage drawInRect:rect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return newImage;
}

- (UIImage*)cropWithRect:(CGRect)cropRect
{
	CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, cropRect);
	UIImage* result = [UIImage imageWithCGImage:imageRef];
	CGImageRelease(imageRef);
	return result;
}

@end
