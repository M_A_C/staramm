//
//  UIImage+TextDrawing.h
//  VimgKeditor
//
//  Created by Илья on 30.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage(TextDrawing)

-(UIImage *)imageWithText:(NSString *)text
				 withFont:(UIFont*)font
					color:(UIColor*)color
				   inRect:(CGRect)rect
				alignment:(NSTextAlignment)alignment;

- (UIImage*)cropWithRect:(CGRect)cropRect;
- (UIImage *)imageWithMaskImage:(UIImage*)maskImage;

@end
