//
//  UIView+Move.h
//  VimgKeditor
//
//  Created by Ilya Sergeev on 01.10.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView(Move)

- (void)moveWithDeltaX:(CGFloat)dx deltaY:(CGFloat)dy;

@end
