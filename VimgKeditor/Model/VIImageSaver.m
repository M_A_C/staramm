//
//  VIImageSaver.m
//  VimgKeditor
//
//  Created by Илья on 21.09.12.
//  Copyright (c) 2012 ilya.sergeev. All rights reserved.
//

#import "VIImageSaver.h"

#import <AssetsLibrary/AssetsLibrary.h>

@implementation VIImageSaver
{
	VICompletionHandlerWithURL _completionBlock;
	VIErrorHandler _errorBlock;
}

+ (VIImageSaver*)saveImage:(UIImage*)image
		   completionBlock:(VICompletionHandlerWithURL)completionBlock
				errorBlock:(VIErrorHandler)errorBlock
{
	return [[VIImageSaver alloc] initWithImage:image
							   completionBlock:completionBlock
									errorBlock:errorBlock];
}

- (id)initWithImage:(UIImage*)image
	completionBlock:(VICompletionHandlerWithURL)completionBlock
		 errorBlock:(VIErrorHandler)errorBlock
{
	self = [super init];
	if (self != nil)
	{
		_completionBlock = completionBlock;
		_errorBlock = errorBlock;
		
		ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
		
		[library writeImageToSavedPhotosAlbum:[image CGImage]
								  orientation:(ALAssetOrientation)[image imageOrientation]
							  completionBlock:^(NSURL *assetURL, NSError *error){
									if (error)
									{
										_errorBlock(error);
									}
									else
									{
										_completionBlock(assetURL);
									}
							  }];
	}
	return self;
}

@end
