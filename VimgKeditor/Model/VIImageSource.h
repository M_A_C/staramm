//
//  VIMaskedImage.h
//  VimgKeditor
//
//  Created by Ilya Sergeev on 01.10.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GPUImage.h"

@interface VIImageSource : NSObject

@property CGRect cropRect;

@property (strong) UIImage* mask;
@property (strong) GPUImageOutput<GPUImageInput>* filter;

@property (readonly) UIImage* image;
@property (readonly, nonatomic) UIImage* preview;
@property (readonly, nonatomic) GPUImagePicture* gpuSource;

- (id)initWithImage:(UIImage*)image;
- (id)initWithImage:(UIImage*)image previewSize:(CGSize)previewSize;

@end
