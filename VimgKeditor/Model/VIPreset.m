//
//  VIPresetsFactory.m
//  VimgKeditor
//
//  Created by Илья on 01.10.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIPreset.h"

#import "UIImage_resizable.h"

@implementation VIPreset

+ (NSArray*)avalablePresets
{
	static NSArray* presets = nil;
    static dispatch_once_t pred;
    
	dispatch_once(&pred, ^{
		presets = @[
		[VIPreset presetWithImage:[UIImage resizableImageWithName:@"Preset_empty"] fontColor:[UIColor whiteColor]],
		[VIPreset presetWithImage:[UIImage resizableImageWithName:@"Preset_Polaroid"] fontColor:[UIColor blackColor]],
		[VIPreset presetWithImage:[UIImage resizableImageWithName:@"Preset_Demotivator"] fontColor:[UIColor whiteColor]],
		[VIPreset presetWithImage:[UIImage resizableImageWithName:@"Preset_vignette"] fontColor:[UIColor whiteColor]]];
	});
    
    return presets;
}

+ (VIPreset*)presetWithImage:(UIImage*)image fontColor:(UIColor*)fontColor
{
	return [[VIPreset alloc] initWithImage:image fontColor:fontColor];
}

- (id)initWithImage:(UIImage*)image fontColor:(UIColor*)fontColor
{
	self = [super init];
	if (self != nil)
	{
		_image = image;
		_fontColor = fontColor;
	}
	return self;
}

@end
