//
//  VICameraPosibilieties.m
//  VimgKeditor
//
//  Created by Илья on 21.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VICameraPosibilieties.h"

#import "GPUImage.h"


@implementation VICameraPosibilieties

+ (NSMutableArray*)devices
{
	static NSMutableArray* devices = nil;
	static dispatch_once_t pred;
    
    if(!devices)
    {
        dispatch_once(&pred, ^{
            devices = [NSMutableArray new];
			for (AVCaptureDevice *device in [AVCaptureDevice devices])
			{
				if ([device hasMediaType:AVMediaTypeVideo])
				{
					[devices addObject:device];
				}
			}
        });
    }
    
    return devices;
}

+ (VICameraPosibilieties*)posibilieties
{
	static VICameraPosibilieties* _posibilieties = nil;
    static dispatch_once_t pred;
    
    if(!_posibilieties)
    {
        dispatch_once(&pred, ^{
            _posibilieties = [VICameraPosibilieties new];
        });
    }
    
    return _posibilieties;
}

+ (BOOL)hasCamera
{
	return self.devices.count > 0;
}

+ (BOOL)hasFewCameras
{
	return self.devices.count > 1;
}

+ (NSArray*)flashModesForDevice:(AVCaptureDevice*)device
{
	NSArray* result = nil;
	if (device.hasFlash)
	{
		NSMutableArray* avalableFlashModes = [NSMutableArray array];
		for (NSNumber* flashMode in @[@(AVCaptureFlashModeAuto), @(AVCaptureFlashModeOn), @(AVCaptureFlashModeOff)])
		{
			if ([device isFlashModeSupported:flashMode.intValue])
			{
				[avalableFlashModes addObject:flashMode];
			}
		}
		result = avalableFlashModes;
	}
	return result;
}

+ (NSString*)stringOfFlashMode:(AVCaptureFlashMode)flashMode
{
	NSString* result = @"";
	switch (flashMode)
	{
		case AVCaptureFlashModeOn:
			result = @"on";
			break;
			
		case AVCaptureFlashModeOff:
			result = @"off";
			break;
			
		case AVCaptureFlashModeAuto:
			result = @"auto";
			break;
	}
	return result;
}

@end
