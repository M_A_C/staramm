//
//  VIImageSaver.h
//  VimgKeditor
//
//  Created by Илья on 21.09.12.
//  Copyright (c) 2012 ilya.sergeev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VIHandlers.h"

@interface VIImageSaver : NSObject

+ (VIImageSaver*)saveImage:(UIImage*)image
		   completionBlock:(VICompletionHandlerWithURL)completionBlock
				errorBlock:(VIErrorHandler)errorBlock;

- (id)initWithImage:(UIImage*)image
	completionBlock:(VICompletionHandlerWithURL)completionBlock
		 errorBlock:(VIErrorHandler)errorBlock;

@end
