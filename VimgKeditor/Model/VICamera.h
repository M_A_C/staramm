//
//  VICamera.h
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GPUImage.h"
#import <AVFoundation/AVFoundation.h>

@interface VICamera : NSObject

+ (VICamera*)sharedCamera;

@property (readonly) GPUImageStillCamera* stillCamera;
@property (readonly) GPUImageOutput* output;

- (AVCaptureDevice*)captureDevice;

- (void)rotateCamera;
- (void)startCapture;
- (void)stopCapture;
- (void)makePhoto:(void (^)(NSData *processedJPEG, NSError *error))completionHandler;
- (void)makePhoto:(void (^)(NSData *processedJPEG, NSError *error))completionHandler
	   withFilter:(GPUImageOutput <GPUImageInput>*)filter;
- (void)setFlashMode:(AVCaptureFlashMode)flashMode;

@end
