//
//  VIFiltersEnum.h
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#ifndef VimgKeditor_VIFiltersEnum_h
#define VimgKeditor_VIFiltersEnum_h

enum GPUFilter {
	GPUFilterEmpty,
	GPUFilterSepia,
	GPUFilterColorInvert,
	GPUFilterGrayscale,
	GPUFilterPrewittEdgeDetection,
	GPUFilterSobelEdgeDetection,
	GPUFilterFalseColor,
	GPUFilterAmatorkaFilter,
	GPUFilterMissEtikate,
	GPUFilterSoftElegance,
	GPUFilterToon,
	GPUFilterEmboss,
	GPUFilterPinchDistortion,
	GPUFilterGausianBlur
};
typedef enum GPUFilter GPUFilter;

#endif
