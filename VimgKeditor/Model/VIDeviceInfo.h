//
//  VIDeviceInfo.h
//  VimgKeditor
//
//  Created by Илья on 29.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AVFoundation/AVFoundation.h>

@interface VIDeviceInfo : NSObject

+ (BOOL)isLowForceDevice;

+ (CGSize)photoSizeForDevicePosition:(AVCaptureDevicePosition)position;

@end
