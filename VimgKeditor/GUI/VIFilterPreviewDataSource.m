//
//  VIFilterPreviewDataSource.m
//  VimgKeditor
//
//  Created by Илья on 27.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIFilterPreviewDataSource.h"

#import "VIFiltersFactory.h"
#import "VIFilterImagePreviewCell.h"

#define CellDirivesSize 10

@implementation VIFilterPreviewDataSource

- (GPUFilter)selectedFilterNumber
{
	return [_filterNumbers[_selectedFilterIndexPath.row] intValue];
}

- (id)init
{
	self = [super init];
	if (self != nil)
	{
		_filterNumbers = [VIFiltersFactory avalableFilters];
		_presets = [VIPreset avalablePresets];
	}
	return self;
}

- (void)setSelectedFilterNumber:(GPUFilter)selectedFilterNumber
{
	for (int i = 0; i < _filterNumbers.count; ++i)
	{
		GPUFilter filterNumber = [_filterNumbers[i] intValue];
		if (filterNumber == selectedFilterNumber)
		{
			_selectedFilterIndexPath = [NSIndexPath indexPathForRow:i inSection:ImageFilter];
			break;
		}
	}
}

- (void)setSelectedPresetIdx:(NSUInteger)selectedPresetIdx
{
	_selectedPresetIndexPath = [NSIndexPath indexPathForItem:selectedPresetIdx inSection:ImagePreset];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	if (_selectedFilterIndexPath == nil)
	{
		self.selectedFilterNumber = GPUFilterEmpty;
	}
	if (!self.showPresets)
	{
		return 1;
	}
	
	if (_selectedPresetIndexPath == nil)
	{
		self.selectedPresetIdx = 0;
	}
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	switch (section)
	{
		case ImageFilter:
			return _filterNumbers.count;
			
		case ImagePreset:
			return _presets.count;
			
		default:
			return 0;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == ImagePreset)
	{
		static NSString *identifier = @"VIFilterImagePreviewCell";
		
		VIFilterImagePreviewCell *cell = (VIFilterImagePreviewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
		
		if (!cell)
		{
			CGAffineTransform rotateTable = CGAffineTransformMakeRotation(M_PI_2);
			NSArray* topLavelsObjs = [[NSBundle mainBundle] loadNibNamed:@"VIFilterImagePreviewCell" owner:nil options:nil];
			
			for (id obj in topLavelsObjs)
			{
				if ([obj isKindOfClass:[VIFilterImagePreviewCell class]])
				{
					cell = obj;
					break;
				}
			}
			cell.mainView.transform = rotateTable;
		}
		
		VIPreset* preset = _presets[indexPath.row];
		cell.image = preset.image;
		cell.didSelected = [_selectedPresetIndexPath isEqual:indexPath];
		
		return cell;
	}
	
	@throw @"abstract method";
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 88;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSIndexPath* deselectedIndexPath = nil;
	switch (indexPath.section)
	{
		case ImageFilter:
			if (indexPath != _selectedFilterIndexPath)
			{
				deselectedIndexPath = _selectedFilterIndexPath;
			}
			_selectedFilterIndexPath = indexPath;
			[self.delegate filtersDataSource:self
							 didSelectFilter:[_filterNumbers[indexPath.row] intValue]];
			break;
			
		case ImagePreset:
			if (indexPath != _selectedPresetIndexPath)
			{
				deselectedIndexPath = _selectedPresetIndexPath;
			}
			_selectedPresetIndexPath = indexPath;
			if ([self.delegate respondsToSelector:@selector(filtersDataSource:didSelectPreset:)])
			{
				[self.delegate filtersDataSource:self
								 didSelectPreset:_presets[indexPath.row]];
			}
			
		default:
			break;
	}
	((VIFilterImagePreviewCell*)[tableView cellForRowAtIndexPath:indexPath]).didSelected = YES;
	((VIFilterImagePreviewCell*)[tableView cellForRowAtIndexPath:deselectedIndexPath]).didSelected = NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if (section == ImagePreset)
	{
		return CellDirivesSize;
	}
	return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
	label.backgroundColor = [UIColor clearColor];
	if (section == ImagePreset)
	{
		label.frame = CGRectMake(0, 0, CellDirivesSize, CellDirivesSize);
		label.backgroundColor = [UIColor clearColor];
		return label;
	}
	
	
	return label;
}

@end
