//
//  VIFiltersDataSource.h
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VIFilterPreviewDataSource.h"
#import "VIFiltersEnum.h"
#import "GPUImage.h"

@interface VIFiltersDataSource : VIFilterPreviewDataSource

@property (unsafe_unretained) IBOutlet id<VIFiltersSelectionDelegate> delegate;
@property (unsafe_unretained, nonatomic) GPUFilter selectedFilterNumber;
@property (strong, nonatomic) GPUImageOutput* imageOutput;

@end


