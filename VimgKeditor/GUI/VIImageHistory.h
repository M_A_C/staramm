//
//  VIImageHistory.h
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VICameraRollViewControllerDelegate.h"

@interface VIImageHistory : NSObject <UITableViewDataSource, UITableViewDelegate>

@property IBOutlet id<VICameraRollViewControllerDelegate> delegate;

@end
