//
//  VIFilterPreviewDataSource.h
//  VimgKeditor
//
//  Created by Илья on 27.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VIFiltersSelectionDelegate.h"
#import "VIFiltersEnum.h"

#define ImageFilter 0
#define ImagePreset 1

@interface VIFilterPreviewDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>
{
@protected
	NSArray* _filterNumbers;
	NSArray* _presets;
	NSIndexPath* _selectedFilterIndexPath;
	NSIndexPath* _selectedPresetIndexPath;
}

@property (unsafe_unretained) IBOutlet id<VIFiltersSelectionDelegate> delegate;
@property (nonatomic) GPUFilter selectedFilterNumber;
@property (nonatomic) NSUInteger selectedPresetIdx;
@property BOOL showPresets;

@end
