//
//  VIFilterImagePreviewDataSource.m
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIFilterImagePreviewDataSource.h"

#import "VIFiltersFactory.h"
#import "VICamera.h"
#import "VIFilterImagePreviewCell.h"

@implementation VIFilterImagePreviewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView.hidden)
	{
		return 0;
	}
	return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == ImagePreset)
	{
		return [super tableView:tableView cellForRowAtIndexPath:indexPath];
	}
	
	static NSString *identifier = @"VIFilterImagePreviewCell";
    
    VIFilterImagePreviewCell *cell = (VIFilterImagePreviewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell)
	{
		CGAffineTransform rotateTable = CGAffineTransformMakeRotation(M_PI_2);
		NSArray* topLavelsObjs = [[NSBundle mainBundle] loadNibNamed:@"VIFilterImagePreviewCell" owner:nil options:nil];
		
		for (id obj in topLavelsObjs)
		{
			if ([obj isKindOfClass:[VIFilterImagePreviewCell class]])
			{
				cell = obj;
				break;
			}
		}
		cell.mainView.transform = rotateTable;
    }
    
	GPUFilter filterNumber = [_filterNumbers[indexPath.row] intValue];
	cell.image = [VIFiltersFactory previewImageForFilter:filterNumber];
	cell.didSelected = [_selectedFilterIndexPath isEqual:indexPath];
    
    return cell;
}

@end
