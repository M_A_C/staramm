//
//  VICameraRollViewController.m
//  VimgKeditor
//
//  Created by Илья on 21.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VICameraRollViewController.h"

#import "VIStorage.h"
#import "VICameraPosibilieties.h"
#import "GPUImage.h"
#import "VICamera.h"
#import "VIFiltersDataSource.h"
#import "VIFilterPreviewTableCell.h"
#import "VIFilterImagePreviewDataSource.h"
#import "VIFiltersFactory.h"
#import "VIDeviceInfo.h"

#define FiltersViewHeight 80
#define FiltersSelectionRectHidden CGRectMake(0, 480, 320, FiltersViewHeight)
#define FiltersSelectionRectAvalable CGRectMake(0, 310, 320, FiltersViewHeight)
#define BlurSliderVissibleRect CGRectMake(18, 80, 284, 23)
#define BlurSliderHiddenRect CGRectMake(18, 40, 284, 23)
#define FilterViewFrame CGRectMake(0, 0, 60, FiltersViewHeight)
#define FilterViewDelta 10

@interface VICameraRollViewController () <VIFiltersSelectionDelegate>

@property (unsafe_unretained, nonatomic) IBOutlet GPUImageView* previewPhotoView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton* changeCameraButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton* flashModeButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *filtersButton;
@property (strong, nonatomic) IBOutlet VIFiltersDataSource *filtersDataSource;
@property (strong, nonatomic) IBOutlet VIFilterImagePreviewDataSource *lowResolutionFiltersDataSource;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *filtersPreview;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *progressCloudView;

@end

@implementation VICameraRollViewController
{
	NSUInteger _currentFlashModeIdx;
	GPUImageOutput<GPUImageInput>* _currentFilter;
	GPUImageOutput<GPUImageInput>* _blurFilter;
	GPUImageFilterPipeline* _pipline;
	GPUFilter _currentFilterNumber;
	VICamera* _camera;
	BOOL _isShowFilters;
}

+ (VICameraRollViewController*)sharedController
{
	static VICameraRollViewController *_sharedController = nil;
    static dispatch_once_t pred;
    
    if(!_sharedController)
    {
        dispatch_once(&pred, ^{
            _sharedController = [VICameraRollViewController new];
        });
    }
    
    return _sharedController;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	_camera = [VICamera sharedCamera];
	_currentFilterNumber = GPUFilterEmpty;
	_currentFilter = [VIFiltersFactory createFilterWithNumber:_currentFilterNumber];
	
	self.previewPhotoView.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
	
	self.filtersPreview.transform = CGAffineTransformMakeRotation(-M_PI_2);
	self.filtersPreview.hidden = true;
	self.filtersPreview.frame = FiltersSelectionRectHidden;
	self.filtersDataSource.showPresets = NO;
	
	if ([VIDeviceInfo isLowForceDevice])
	{
		self.filtersPreview.dataSource = self.lowResolutionFiltersDataSource;
		self.filtersPreview.delegate = self.lowResolutionFiltersDataSource;
	}
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	_pipline = [[GPUImageFilterPipeline alloc] initWithOrderedFilters:@[_currentFilter]
																input:_camera.output
															   output:self.previewPhotoView];
	
	if (_blurFilter != nil)
	{
		[_pipline addFilter:(GPUImageFilter*)_blurFilter atIndex:0];
	}
	
	_blurFilter = nil;
	self.progressCloudView.hidden = YES;
	_filtersDataSource.imageOutput = _camera.output;
	
	[_camera startCapture];
	
	self.changeCameraButton.hidden = !VICameraPosibilieties.hasFewCameras;
	
	_isShowFilters = NO;
	self.filtersButton.selected = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	[_camera stopCapture];
	[self stopFilters];
	
	[_pipline removeAllFilters];
	_pipline = nil;
	[_camera.output removeAllTargets];
	[_currentFilter removeAllTargets];
}

- (void)viewDidUnload
{
	[self setPreviewPhotoView:nil];
	[self setChangeCameraButton:nil];
	[self setFlashModeButton:nil];
	[self setFiltersButton:nil];
	[self setFiltersDataSource:nil];
	[self setFiltersPreview:nil];
	[self setLowResolutionFiltersDataSource:nil];
	
	[self setProgressCloudView:nil];
	[super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (IBAction)cancelAction:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)makePhotoButton:(id)sender
{	
	[self stopFilters];
	GPUImageOutput <GPUImageInput>* filter = nil;
	if (_blurFilter != nil)
	{
		filter = [VIFiltersFactory createFilterWithNumber:GPUFilterGausianBlur];
		[filter forceProcessingAtSize:self.previewPhotoView.sizeInPixels];
	}
	[_camera makePhoto:^(NSData *processedJPEG, NSError *error){
		
		[_camera.stillCamera stopCameraCapture];
		UIImage *image = [[UIImage alloc] initWithData:processedJPEG];
		[self.delegate controller:self didPickImage:image withFilter:_currentFilterNumber];
		if (error != nil)
		{
			NSLog(@"can not save image, error = %@", error);
			[self.navigationController popViewControllerAnimated:YES];
			self.progressCloudView.hidden = YES;
		}
	} withFilter:filter];
	self.progressCloudView.hidden = NO;
}

- (IBAction)changeCameraAction:(id)sender
{
	[_camera rotateCamera];
	[self refreshDeviceInfo];
}

- (IBAction)changeFlashModeAction:(id)sender
{
	_currentFlashModeIdx++;
	
	NSArray* flashModes = [VICameraPosibilieties flashModesForDevice:_camera.captureDevice];

	if (_currentFlashModeIdx >= flashModes.count)
	{
		_currentFlashModeIdx = 0;
	}
	if (flashModes.count > 0)
	{
		[self changeFlashModeTo:[flashModes[_currentFlashModeIdx] intValue]];
	}
}


- (IBAction)changeBlurAction:(UIButton*)senderButton
{
	BOOL isBlurEnable = !senderButton.selected;
	senderButton.selected = isBlurEnable;
	
	if (isBlurEnable)
	{
		_blurFilter = [VIFiltersFactory createFilterWithNumber:GPUFilterGausianBlur];
		[_blurFilter prepareForImageCapture];
		[_pipline addFilter:(GPUImageFilter*)_blurFilter atIndex:0];
	}
	else
	{
		[_pipline removeFilterAtIndex:0];
		_blurFilter = nil;
	}
}

- (IBAction)showFiltersAction:(id)sender
{
	_isShowFilters = !_isShowFilters;
	CGRect filtersRect = FiltersSelectionRectHidden;
	if (_isShowFilters)
	{
		filtersRect = FiltersSelectionRectAvalable;
	}
	self.filtersButton.selected  = _isShowFilters;
	self.filtersPreview.hidden = NO;
	if (_isShowFilters)
	{
		[self.filtersPreview reloadData];
	}
	
	[UIView animateWithDuration:0.5 animations:^{
		self.filtersPreview.frame = filtersRect;
	} completion:^(BOOL finished) {
		self.filtersPreview.hidden = !_isShowFilters;
		[self stopFilters];
	}];
}

- (void)stopFilters
{
	NSArray* vissibleCells = self.filtersPreview.visibleCells;
	for (UITableViewCell* cell in vissibleCells)
	{
		if ([cell isKindOfClass:[VIFilterPreviewTableCell class]])
		{
			[(VIFilterPreviewTableCell*)cell turnFilter:_isShowFilters];
		}
	}
	if (!_isShowFilters)
	{
		[self.filtersPreview reloadData];
	}
}

- (void)refreshDeviceInfo
{
	AVCaptureDevice* device = _camera.captureDevice;
	_currentFlashModeIdx = 0;
	NSArray* flashModes = [VICameraPosibilieties flashModesForDevice:device];
	
	self.flashModeButton.hidden = YES;
	if (device.hasFlash)
	{
		self.flashModeButton.hidden = NO;
		[self changeFlashModeTo:[flashModes[_currentFlashModeIdx] intValue]];
	}
}

- (void)changeFlashModeTo:(AVCaptureFlashMode)flashMode
{
	[_camera setFlashMode:flashMode];
	
	NSString* buttonTitle = [VICameraPosibilieties  stringOfFlashMode:flashMode];
	[self.flashModeButton setTitle:buttonTitle forState:UIControlStateNormal];
}

#pragma mark - VIFiltersSelectionDelegate

- (void)filtersDataSource:(VIFiltersDataSource*)dataSource didSelectFilter:(GPUFilter)filterNumber
{
	_currentFilterNumber = filterNumber;
	_currentFilter = [VIFiltersFactory createFilterWithNumber:_currentFilterNumber];
	
	[_pipline removeAllFilters];
	if (_blurFilter != nil)
	{
		[_pipline addFilter:(GPUImageFilter*)_blurFilter];
	}
	[_pipline addFilter:(GPUImageFilter*)_currentFilter];
}

@end
