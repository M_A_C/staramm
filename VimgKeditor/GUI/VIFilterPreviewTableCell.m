//
//  VIFilterPreviewTableCell.m
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIFilterPreviewTableCell.h"

#import <QuartzCore/QuartzCore.h>
#import "VIFiltersFactory.h"

@implementation VIFilterPreviewTableCell
{
	GPUImageOutput<GPUImageInput>* _filter;
}

- (void)awakeFromNib
{
	self.mainView.layer.cornerRadius = 7;
	self.previewView.fillMode = kGPUImageFillModeStretch;
}

- (void)setImageOutput:(GPUImageOutput *)imageOutput
{
	if (_imageOutput == imageOutput)
	{
		return;
	}
	[_imageOutput removeTarget:_filter];
	_imageOutput = imageOutput;
	[self turnOnFilter];
}

- (void)createFilter
{
	[_filter removeAllTargets];
	if (_filter != nil)
	{
		[_imageOutput removeTarget:_filter];
	}
	[_filter forceProcessingAtSize:self.previewView.sizeInPixels];
}

- (GPUImageOutput<GPUImageInput>*)filter
{
	if (_filter == nil)
	{	
		[self createFilter];
	}
	return _filter;
}

- (void)setFilter:(GPUImageOutput<GPUImageInput> *)filter
{
	if (filter == _filter)
	{
		return;
	}
	
	[self turnOffFilter];
	_filter = filter;
	[_filter forceProcessingAtSize:self.previewView.sizeInPixels];
	[self turnOnFilter];

}

- (void)turnFilter:(BOOL)onOrOff
{
	if (onOrOff)
	{
		[self turnOnFilter];
	}
	else
	{
		[self turnOffFilter];
	}
}

- (void)turnOnFilter
{
	if (_filter != nil)
	{
		[self.imageOutput addTarget:_filter];
		[_filter addTarget:self.previewView];
	}
}

- (void)turnOffFilter
{
	[self.imageOutput removeTarget:_filter];
	[_filter removeTarget:self.previewView];
}

- (void)setDidSelected:(BOOL)didSelected
{
	_didSelected = didSelected;
	self.maskView.highlighted = _didSelected;
}

@end
