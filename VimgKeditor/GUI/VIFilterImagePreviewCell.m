//
//  VIFilterImagePreviewCell.m
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIFilterImagePreviewCell.h"

#import <QuartzCore/QuartzCore.h>

#define BasicImageName @"BasicImage"

@implementation VIFilterImagePreviewCell

- (void)awakeFromNib
{
	[super awakeFromNib];
	self.mainView.layer.cornerRadius = 7;
	self.previewView.image = self.image;
}

- (void)setImage:(UIImage *)image
{
	if (image == _image)
	{
		return;
	}
	_image = image;
	self.previewView.image = self.image;
}

- (void)setDidSelected:(BOOL)didSelected
{
	_didSelected = didSelected;
	self.borderImageView.highlighted = didSelected;
}

@end
