//
//  VICameraRollViewControllerDelegate.h
//  VimgKeditor
//
//  Created by Илья on 26.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VIFiltersEnum.h"

@protocol VICameraRollViewControllerDelegate <NSObject>

- (void)controller:(UIViewController*)controller
	  didPickImage:(UIImage*)image
		withFilter:(GPUFilter)filterNumber;

@end
