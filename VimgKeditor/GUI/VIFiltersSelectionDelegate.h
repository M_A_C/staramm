//
//  VIFiltersSelectionDelegate.h
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VIFiltersEnum.h"
#import "VIPreset.h"

@protocol VIFiltersSelectionDelegate <NSObject>

- (void)filtersDataSource:(id)dataSource didSelectFilter:(GPUFilter)filterNumber;

@optional
- (void)filtersDataSource:(id)dataSource didSelectPreset:(VIPreset*)preset;

@end
